import argparse
from asyncio.subprocess import DEVNULL
import os
import sys
import xml.etree.ElementTree as XET
import datetime
import numpy as np
import random
import math
import subprocess
import glob
import json

def extract_frame_date_time_original(file_name):
	# result = subprocess.run(['exiftool','-XMP:DateTimeOriginal', file_name], encoding='utf-8',stdout=subprocess.PIPE)
    cmd = ['exiv2', '-K', 'Xmp.exif.DateTimeOriginal', file_name ]

    result = subprocess.run(cmd, encoding='utf-8',stdout=subprocess.PIPE, stderr=DEVNULL)
    if result is None:
        print('could not extract exif from file {}'.format(file_name))
        return {}
    res = result.stdout.strip().split('\n')
    if res == '':
        return {}
    for resval in res:
        rs = resval.split()
        if rs == None:
            return {}
        if rs[0] == 'Xmp.exif.DateTimeOriginal':
            time_string = rs[3]
            ftime = datetime.datetime.strptime(time_string,'%Y-%m-%dT%H:%M:%S.%f')
            return ftime

def get_gps_times(gps_file):
    tree = XET.parse(gps_file)
    root = tree.getroot()
    time_array = []
    for trkpt in root.iter('{http://www.topografix.com/GPX/1/0}trkpt'):
        for ch in trkpt.getchildren():
            if ch.tag == '{http://www.topografix.com/GPX/1/0}time':
                # print( ch.text )
                ta = datetime.datetime.strptime(ch.text,'%Y-%m-%dT%H:%M:%S.%fZ')
                ta += datetime.timedelta( seconds=random.randint(0,5), milliseconds=random.randint(0,100) )
                # ta += datetime.timedelta( seconds=0, milliseconds=1 )
                time_array.append(ta)
    return sorted(time_array)

class FData:
    name = None
    sample_time = None
    sample_duration = 0.0
    accelerometer = []
    acc_std = []
    acc_mean = []
    time = None

def is_video_timewarp(xroot):
    rate = ""
    for x in xroot.iter('{http://ns.exiftool.ca/QuickTime/GoPro/1.0/}Rate'):
        rate = x.text
        break
    if rate == '2_1SEC':
        return False
    return True

def get_metadata_namespace(xroot):
    for domain in ['org', 'ca']:
        for id in range(1,7):
            qnamespace = "{http://ns.exiftool."+domain+"/QuickTime/Track"+str(id)+"/1.0/}"
            qtag = qnamespace + 'Accelerometer'
            for x in xroot.iter(qtag):
                if x.tag == qtag:
                    return qnamespace
    return None

def get_frame_data(xml_file):

    meta_tree = XET.parse(xml_file)
    root = meta_tree.getroot()

    SFrameData = {}
    frame_sample_time = None
    cur_frame_data = None

    acc_namespace = get_metadata_namespace(root)
    if acc_namespace is None:
        print( 'WARNING: Could not find metadata namespace in the XML file')
        return {}

    for child in root:
        for elem in child.iter():
            if elem.tag != acc_namespace+'SampleTime' and (cur_frame_data == None):
                continue

            if elem.tag == acc_namespace+'SampleTime':
                frame_sample_time = elem.text
                SFrameData[frame_sample_time] = FData()
                cur_frame_data = SFrameData[frame_sample_time]
                cur_frame_data.sample_time = frame_sample_time
            elif elem.tag == acc_namespace+'Accelerometer':
                stxt = elem.text.strip().replace("\n","").split()
                cur_frame_data.accelerometer = np.array( stxt, dtype=np.float )
                cur_frame_data.accelerometer = cur_frame_data.accelerometer.reshape( (len(cur_frame_data.accelerometer)//3, 3) )
                cur_frame_data.acc_std = np.std(cur_frame_data.accelerometer,axis=0)
                cur_frame_data.acc_mean = np.mean(cur_frame_data.accelerometer,axis=0)
            elif elem.tag == acc_namespace+'GPSDateTime':
                cur_frame_data.time = datetime.datetime.strptime(elem.text,'%Y:%m:%d %H:%M:%S.%f')
            elif elem.tag == acc_namespace+'SampleDuration':
                cur_frame_data.sample_duration = float( (elem.text).replace('s','') )

    # format the dictionary based on time

    # rehash frames based on their gpsdatetime value
    frames = {}
    for key, fobj in SFrameData.items():
        frames[fobj.time] = fobj
        # print( '%10.4f %10.4f %10.4f | %10.4f %10.4f %10.4f' %
        #      (fobj.acc_mean[0],  fobj.acc_mean[1], fobj.acc_mean[2],
        #      fobj.acc_std[0],  fobj.acc_std[1], fobj.acc_std[2] ) )

    return frames

def positive_difference(ref_time, qry_time):
    duration = (qry_time - ref_time).total_seconds()
    if duration < 0:
        duration = 1e10
    return duration

def interpolate_acceleration(frame_data, query_time):

    adata = frame_data.accelerometer
    n_samples = len(adata)
    if n_samples == 1:
        return adata[0,:]

    delta_time = (query_time - frame_data.time).total_seconds()
    if delta_time > frame_data.sample_duration:
        print('query time is longer than the sample duration')
        return None
    if delta_time < 0:
        print('query time is behind sample range')
        return None

    tstep = frame_data.sample_duration / (n_samples-1)
    idx = math.floor(delta_time / tstep)
    if idx == n_samples-1:
        return adata[n_samples-1,:]
    alpha = (delta_time/tstep - idx)
    acc = ( (1.0-alpha) * adata[idx,:] + alpha * adata[idx+1,:])
    return acc

def save_dictionary_in_json(file_name, jdata):
    with open(file_name, 'w') as fp:
        json.dump(jdata, fp,  indent=4)

def get_accelerometer_data(metadata_file, image_folder):

    frames = get_frame_data(metadata_file)

    if not os.path.isdir(image_folder):
        print("ERROR: could not find images folder")
        return None

    time_data = {}
    for imfile in sorted( glob.glob(os.path.join(image_folder,"*.jpg")) ):
        td = extract_frame_date_time_original(imfile)
        time_data[imfile] = td

    acc_data = {}
    for frame_name, frame_time  in time_data.items():
        fv = frames.get(frame_time, frames[min(frames.keys(), key=lambda k: positive_difference(k,frame_time) )])
        acc = interpolate_acceleration(fv, frame_time)
        if acc is None:
            continue
        # Go PRO 360 stores accelerometer data with inverted y - compensating for it. ACCL is stored wrt the Camera referential.
        # To go back to world, apply inverse camera rotation matrix
        acc_data[ os.path.basename(frame_name) ] = list( [ acc[0], -acc[1], acc[2] ]  )

    return acc_data

#
# assumes paths are given relative to the work_folder
#
def save_accelerometer_data(work_folder, image_folder, metadata_file, acc_file):

    metadata_file = os.path.join(work_folder,metadata_file)
    if not os.path.isfile(metadata_file):
        metadata_file = os.path.join(work_folder, "metadata", metadata_file)
        if not os.path.isfile(metadata_file):
            print("ERROR: could not file metadata file")
            return False

    image_folder = os.path.join(work_folder,image_folder)
    if not os.path.isdir(image_folder):
        print("ERROR: could not find images folder")
        return False

    acc_data = get_accelerometer_data(metadata_file, image_folder)
    if acc_data is None:
        return False
    acc_file = os.path.join( work_folder, "accelerometer.json" )
    save_dictionary_in_json(acc_file, acc_data)
    return True

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='parse gopro max xml metadata - interpolate accelerometer for time tags')

    parser.add_argument( '--work-folder', '-wf', type=str, help='main workfolder with', default="/tmp/axis/")
    parser.add_argument( '--metadata-file', '-f',  type=str, help='xml file for metadata', default="metadata.xml" )
    parser.add_argument( '--images-folder', '-if',  type=str, help='image folder', default="images" )

    args, unknown_args = parser.parse_known_args()
    if len(unknown_args) != 0:
        parser.print_help()
        sys.exit(1)

    script_dir = os.path.dirname(os.path.realpath(__file__))

    metadata_file = os.path.join(args.work_folder,args.metadata_file)
    if not os.path.isfile(metadata_file):
        metadata_file = os.path.join(args.work_folder, "metadata", args.metadata_file)
        if not os.path.isfile(metadata_file):
            print("ERROR: could not file metadata file")
            sys.exit(1)

    frames = get_frame_data(os.path.join(metadata_file))
    image_folder = os.path.join(args.work_folder,args.images_folder)
    if not os.path.isdir(image_folder):
        print("ERROR: could not find images folder")
        sys.exit(1)

    acc_data = get_accelerometer_data(metadata_file, image_folder)

    acc_file = os.path.join( args.work_folder, "accelerometer.json" )
    save_dictionary_in_json(acc_file, acc_data)
