FROM ubuntu:18.04 as BUILDER

RUN apt-get update -qq
RUN apt-get install -y git make gcc ffmpeg exiftool exiv2 libjpeg-turbo8-dev python3 python3-pip
RUN python3 -m pip install numpy 
RUN apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /code
COPY setup.sh ./
RUN bash ./setup.sh

WORKDIR /code
COPY *.py gpx.fmt ./

VOLUME /external/in/
VOLUME /external/out/

ENTRYPOINT ["python3", "-u", "run.py"]
CMD = ["-h"]

