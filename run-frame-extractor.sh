#!/bin/bash

set -e

if [ $# -lt 1 ]; then
    echo "usage: input_folder [(OPTIONAL) fps]"
    exit
fi

input_folder="$(realpath "$1")"

if [ ! -d "${input_folder}" ]; then
    echo "usage: input_folder [(OPTIONAL) fps]"
    exit
fi

fps=1
if [ $# -eq 2 ]; then
    fps=$2
fi

docker container run \
    -v /etc/passwd:/etc/passwd:ro -v /etc/group:/etc/group:ro --user $(id -u):$(id -g) \
    -v "${input_folder}":/external/in/ \
    --rm \
    --name frame-extractor \
    360-frame-extractor --fps ${fps}
