import argparse
import os
import sys
import glob
import subprocess

def get_first_file(folder,file_pattern):
    found_file = ''
    for f in glob.glob( folder+"/"+file_pattern ):
        found_file = f
        break
    return found_file

def run_command(cmd, show_progress=False, env=None):
    if show_progress is False:
        rinfo = subprocess.run(cmd, shell=True, stdout=open(os.devnull, "wb"), env=env)
    else:
        rinfo = subprocess.run(cmd, shell=True, env=env)
    return (rinfo.returncode == 0)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='process a dataset')
    parser.add_argument('--video-file', '-vf', type=str, help='a 360 video file', default=None)
    parser.add_argument('--output-folder', '-of', type=str, help='output folder', default="/tmp/vframes/")
    parser.add_argument('--fps', type=int, help='output folder', default=1)

    script_dir = os.path.dirname(os.path.realpath(__file__))

    # env = os.environ.copy()
    args, unknown_args = parser.parse_known_args()
    if args.video_file is None:
        parser.print_help()
        sys.exit(1)

    video_file = args.video_file
    output_folder = args.output_folder
    fps = args.fps

    assert( fps > 0 )

    if not os.path.isfile(video_file):
        print(f'could not find a video file {video_file}')
        sys.exit(1)
    print( f'vfile: {video_file}')

    cmd = f'python3 {script_dir}/extract-frames.py -vf "{video_file}" -of "{output_folder}" -fps {fps}'
    print( cmd )
    run_command(cmd)

