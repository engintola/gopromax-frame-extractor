import argparse
import json
import os
import sys
import glob
import subprocess
from pathlib import Path

def fuse_accelerometer_json(accelerometer_json_files, output_folder):
    merged = {}
    for prefix, json_file in accelerometer_json_files.items():
        json_content = {}
        try:
            with open(json_file) as f:
                json_content = json.load(f)
                f.close()
        except OSError:
            print(f'Warning: could not read accelerometer file {json_file}')

        for image_filename, accel_data in json_content.items():
            merged[f'{prefix}_{image_filename}'] = accel_data

    with open(output_folder + '/accelerometer.json', "w") as f:
        json.dump(merged, f)
        f.close()


def run_command(cmd, show_progress=False, env=None):
    if show_progress is False:
        rinfo = subprocess.run(cmd, shell=True, stdout=open(os.devnull, "wb"), env=env)
    else:
        rinfo = subprocess.run(cmd, shell=True, env=env)
    return (rinfo.returncode == 0)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='process a dataset')
    parser.add_argument('--input-folder', '-if', type=str, help='folder containing a 360 video file', default="/external/in")
    parser.add_argument('--output-folder', '-of', type=str, help='output folder', default=None)
    parser.add_argument('--fps', type=float, help='fps', default=1)

    script_dir = os.path.dirname(os.path.realpath(__file__))

    env = os.environ.copy()
    # env["OMP_NUM_THREADS"] = str(max_threads)

    args, unknown_args = parser.parse_known_args()
    if args.input_folder is None:
        parser.print_help()
        sys.exit(1)

    fps = args.fps
    input_folder  = args.input_folder
    output_folder = args.output_folder
    if output_folder is None:
        output_folder = os.path.join(input_folder,"image_data/images")

    num_processed = 0
    accelerometer_json_files = {}

    for video_file in glob.glob( input_folder+"/*.360" ):
        num_processed += 1
        prefix = f'{os.path.basename(video_file)}_frames'
        subfolder = os.path.join(output_folder, prefix)

        accelerometer_json_files[prefix] = subfolder + '/accelerometer.json'

        Path(subfolder).mkdir(parents=True, exist_ok=True)

        source_file_info = subprocess.check_output(['ls', '-l', video_file]).decode("utf-8")

        cache_info_file = os.path.join(subfolder, 'cache.info')
        try:
            with open(cache_info_file, "r") as f:
                if source_file_info == f.read():
                    print(f'File {video_file} has been processed already, skipping.')
                    continue
        except FileNotFoundError:
            pass

        print( f'ifolder: {input_folder}')
        print( f'ofolder: {subfolder}')
        print( f'vfile: {video_file}')
        print('')

        cmd = f'python3 -u "{script_dir}/extract-frames.py" -vf "{video_file}" -of "{subfolder}" -fps "{fps}"'
        print( cmd )
        if run_command(cmd, show_progress=True):
            with open(cache_info_file, "w") as f:
                f.write(source_file_info)
                f.close()

        # extract-frames.py generates in subfolder/images/*, but for opensfm to
        # pick images, they have to be in output_folder.
        for image in glob.glob(subfolder+"/images/*.jpg"):
            os.rename(image, os.path.join(output_folder, f'{prefix}_{os.path.basename(image)}'))

    fuse_accelerometer_json(accelerometer_json_files, output_folder + '/..')

    if num_processed == 0:
        print(f'could not find any video file in {input_folder}')
        sys.exit(1)
